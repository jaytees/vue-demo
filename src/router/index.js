import VueRouter from 'vue-router';

const router = new VueRouter({
  // tells it to use browserRouter instead of hashRouter
  mode: 'history',
  routes: []
});

export default router;
