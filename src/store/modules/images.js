// initialise state values for first load
const state = {
  images: []
};
// retrieve data from state
// naming convetion get...
const getters = {
  getImages: state => state.images
  //   getImagesById: (state, id) => {
  //     state.images.forEach(image => {});
  //   }
};
// set data into state
// naming convention set...
// function that operates on state makes one distinct change
//     - very explicit
const mutations = {
  setImages: (state, images) => {
    state.images = images;
  }
};
// Functions, that perform as task by assembling together multiple mutations to make change to state
const actions = {
  // from api
  async fetchImages(context, searchTerm) {
    const response = await fetch(`https://api.unsplash.com/search/photos?query=${searchTerm}`, {
      headers: {
        Authorization: `Client-ID ${process.env.VUE_APP_UNSPLASH_CLIENT_ID}`
      }
    }).then(function(response) {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response.json();
    });

    context.commit('setImages', response.results);
  },
  clearImages({ commit }) {
    commit('setImages', []);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
