import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store';
import router from './router';
import App from './App';

Vue.use(VueRouter);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
